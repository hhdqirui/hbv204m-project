Closes #

### MR Checklist

#### MR Creator
Ensure that you have:
- [ ] Read and understood our PR guideline
    - [ ] Added the issue number to the "Closes" keyword above
    - [ ] Titled the PR as specified in the abovementioned document
- [ ] Made your changes on a branch other than `master` and `release`
- [ ] Gone through all the changes in this PR and ensured that:
    - [ ] They addressed one (and only one) issue
    - [ ] No unintended changes were made
- [ ] Run and passed CI pipelines
- [ ] Added/updated tests, if changes in functionality were involved
- [ ] Added/updated documentation to public APIs (classes, methods, variables), if applicable

**Outline of Solution**
<!-- Tell us how you solved the issue. -->
<!-- If there are things you want the reviewers to focus on, include them here as well. -->
<!-- This portion can be skipped if the fix is trivial. -->

#### Reviewer
- [ ] Pass CI pipelines
- [ ] Conform to coding standard
- [ ] Address the issue
