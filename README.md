# HBV204M Project

[![pipeline status](https://gitlab.com/hhdqirui/hbv204m-project/badges/master/pipeline.svg)](https://gitlab.com/hhdqirui/hbv204m-project/-/commits/master)
[![coverage report](https://gitlab.com/hhdqirui/hbv204m-project/badges/master/coverage.svg)](https://gitlab.com/hhdqirui/hbv204m-project/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=hhdqirui_hbv204m-project&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=hhdqirui_hbv204m-project)


This project is for the assignment of HBV204M.  
URL of the website of this project: [https://hhdqirui.gitlab.io/hbv204m-project/](https://hhdqirui.gitlab.io/hbv204m-project/)

## Building this project
Maven is used to automate the build of this project.

To compile the Java classes, run the command line `mvn compile` in the root directory of this project.

More details can be found [here](https://gitlab.com/hhdqirui/hbv204m-project/-/wikis/Software-Quality-Management-Plan#build-automation)

## Testing
The code for testing reside in the directory `src/test/java`. 

To run the tests, run the command line `mvn test` in the root directory of this project.

## Code coverage report
The code coverage report will be available in `target/site/jacoco/index.html`. The report is automatically generated after running the tests using the `mvn test` command.

It can also be accessed from the [GitLab page for code coverage report](https://hhdqirui.gitlab.io/hbv204m-project/jacoco/index.html)
