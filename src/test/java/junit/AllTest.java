package junit;

import junit.framework.AssertTest;
import junit.framework.ComparisonCompactorTest;
import junit.runner.SorterTest;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SelectClasses;

@Suite
@SelectClasses({ AssertTest.class, ComparisonCompactorTest.class, SorterTest.class})
public class AllTest {

}