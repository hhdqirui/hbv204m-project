package junit.framework;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.format;
import static junit.framework.Assert.fail;

public class AssertTest {
    @Test
    public void testAssertTrueWithOnlyCondition() {
        // Success
        assertDoesNotThrow(() -> assertTrue(true));

        // Fail
        assertThrows(AssertionFailedError.class, () -> assertTrue(false));
    }

    @Test
    public void testAssertTrueWithMessageAndCondition() {
        // Success
        assertDoesNotThrow(() -> assertTrue("message", true));

        // Fail
        assertThrows(AssertionFailedError.class, () -> assertTrue("message", false));
    }

    @Test
    public void testAssertFalseWithOnlyCondition() {
        // Success
        assertDoesNotThrow(() -> assertFalse(false));

        // Fail
        assertThrows(AssertionFailedError.class, () -> assertFalse(true));
    }

    @Test
    public void testAssertFalseWithMessageAndCondition() {
        // Success
        assertDoesNotThrow(() -> assertFalse("message", false));

        // Fail
        assertThrows(AssertionFailedError.class, () -> assertFalse("message", true));
    }

    @Test
    public void testFail() {
        // Empty message
        assertThrows(AssertionFailedError.class, () -> fail(""));

        // Non-empty message
        assertThrows(AssertionFailedError.class, () -> fail("error message"));
    }

    @Test
    public void testFormat() {
        // With message
        String message = "test:";

        int integer1 = 0;
        int integer2 = 1;
        String expected = "test: expected:<0> but was:<1>";
        String actual = format(message, integer1, integer2);
        assertEquals(expected, actual);

        String string1 = "abc";
        String string2 = "def";
        expected = "test: expected:<abc> but was:<def>";
        actual = format(message, string1, string2);
        assertEquals(expected, actual);

        // Without message
        expected = "expected:<0> but was:<1>";
        actual = format(null, integer1, integer2);
        assertEquals(expected, actual);

        expected = "expected:<abc> but was:<def>";
        actual = format(null, string1, string2);
        assertEquals(expected, actual);
    }
}
