package junit.framework;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ComparisonCompactorTest {

    @Test
    public void testCompact() {
        // context length is 0, expected string is a prefix of the actual string
        ComparisonCompactor comparisonCompactor = new ComparisonCompactor(0, "abcd", "abcdef");
        assertEquals("expected:<...[]> but was:<...[ef]>", comparisonCompactor.compact(null));

        // context length is 0, expected string is a suffix of the actual string
        comparisonCompactor = new ComparisonCompactor(0, "cdef", "abcdef");
        assertEquals("expected:<[]...> but was:<[ab]...>", comparisonCompactor.compact(null));

        // context length is 0, actual string is a prefix of the expected string
        comparisonCompactor = new ComparisonCompactor(0, "abcdef", "abcd");
        assertEquals("expected:<...[ef]> but was:<...[]>", comparisonCompactor.compact(null));

        // context length is 0, actual string is a suffix of the expected string
        comparisonCompactor = new ComparisonCompactor(0, "abcdef", "cdef");
        assertEquals("expected:<[ab]...> but was:<[]...>", comparisonCompactor.compact(null));

        // context length is 0, expected string is not a prefix or suffix of the actual string
        comparisonCompactor = new ComparisonCompactor(0, "cd", "abcdef");
        assertEquals("expected:<[cd]> but was:<[abcdef]>", comparisonCompactor.compact(null));

        // context length is 0, actual string is not a prefix or suffix of the expected string
        comparisonCompactor = new ComparisonCompactor(0, "abcdef", "cd");
        assertEquals("expected:<[abcdef]> but was:<[cd]>", comparisonCompactor.compact(null));

        // context length is less than length of the shorter string,
        // actual string is a prefix of the expected string
        comparisonCompactor = new ComparisonCompactor(3, "abcdef", "abcd");
        assertEquals("expected:<...bcd[ef]> but was:<...bcd[]>", comparisonCompactor.compact(null));

        // context length is less than length of the shorter string,
        // actual string is a suffix of the expected string
        comparisonCompactor = new ComparisonCompactor(3, "abcdef", "cdef");
        assertEquals("expected:<[ab]cde...> but was:<[]cde...>", comparisonCompactor.compact(null));

        // context length is less than length of the shorter string,
        // expected string is a prefix of the actual string
        comparisonCompactor = new ComparisonCompactor(3, "abcd", "abcdef");
        assertEquals("expected:<...bcd[]> but was:<...bcd[ef]>", comparisonCompactor.compact(null));

        // context length is less than length of the shorter string,
        // expected string is a suffix of the actual string
        comparisonCompactor = new ComparisonCompactor(3, "cdef", "abcdef");
        assertEquals("expected:<[]cde...> but was:<[ab]cde...>", comparisonCompactor.compact(null));

        // context length is less than length of the shorter string,
        // expected string is not a prefix or suffix of the actual string
        comparisonCompactor = new ComparisonCompactor(2, "abcdefgh", "abcddfgh");
        assertEquals("expected:<...cd[e]fg...> but was:<...cd[d]fg...>", comparisonCompactor.compact(null));

        // context length is less than length of the shorter string,
        // actual string is not a prefix or suffix of the expected string
        comparisonCompactor = new ComparisonCompactor(2, "abcddfgh", "abcdefgh");
        assertEquals("expected:<...cd[d]fg...> but was:<...cd[e]fg...>", comparisonCompactor.compact(null));

        // expected string is null
        comparisonCompactor = new ComparisonCompactor(3, null, "abcdef");
        assertEquals("expected:<null> but was:<abcdef>", comparisonCompactor.compact(null));

        // expected string is null
        comparisonCompactor = new ComparisonCompactor(3, "abcdef", null);
        assertEquals("expected:<abcdef> but was:<null>", comparisonCompactor.compact(null));

        // expected and actual strings are equal
        comparisonCompactor = new ComparisonCompactor(3, "abcdef", "abcdef");
        assertEquals("expected:<abcdef> but was:<abcdef>", comparisonCompactor.compact(null));
    }
}
