package junit.runner;

import java.util.Vector;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import junit.runner.Sorter.Swapper;

public class SorterTest {

    @Test
    public void testSortStrings () {
        Vector<String> originalVector = new Vector<>();
        originalVector.add("c");
        originalVector.add("b");
        originalVector.add("a");
        originalVector.add("d");

        // Sort the whole vector
        Vector<String> v = new Vector<>();
        Vector<String> vec = new Vector<>();
        for (String s : originalVector) {
            v.addElement(s);
            vec.addElement(s);
        }
        Swapper swapper = new MockParallelSwapper(v);
        Sorter.sortStrings(vec, 0, vec.size() - 1, swapper);
        assertEquals("[a, b, c, d]", vec.toString());

        // Sort part of the vector
        v = new Vector<>();
        vec = new Vector<>();
        for (String s : originalVector) {
            v.addElement(s);
            vec.addElement(s);
        }
        swapper = new MockParallelSwapper(v);
        Sorter.sortStrings(vec, 1, vec.size() - 2, swapper);
        assertEquals("[c, a, b, d]", vec.toString());
    }

    private static class MockParallelSwapper implements Sorter.Swapper {
        Vector fOther;

        MockParallelSwapper(Vector other) {
            fOther= other;
        }
        public void swap(Vector values, int left, int right) {
            Object tmp= values.elementAt(left);
            values.setElementAt(values.elementAt(right), left);
            values.setElementAt(tmp, right);
            Object tmp2= fOther.elementAt(left);
            fOther.setElementAt(fOther.elementAt(right), left);
            fOther.setElementAt(tmp2, right);
        }
    }
}
